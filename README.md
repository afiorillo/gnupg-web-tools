# GnuPG Web Tools

Python wrappers for encrypting web requests using GnuPG

## Overview

Provide a set of decorators, Flask extension, Starlette extension, etc that allows clients to encrypt requests.
During any request, if a client includes `X-GNUPG-CLIENT-KEY` header then responses will be encrypted with GPG.
For POST/PUT requests, the client can also request OPTIONS to check the `X-GNUPG-SERVER-KEY` header.
If the client passes the `X-GNUPG-SIGNATURE` header (with their signed message as payload) then the message will be decrypted before handled by the underlying app.

## Todo

- [ ] Flask decorators
- [ ] Flask extension
- [ ] Starlette/FastAPI
- [ ] Fancy Pypi deployment with options like `gnupg-web-tools[flask]`
- [ ] Client-side / requests wrapper